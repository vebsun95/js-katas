function countBoomerangs(array) {
    if(!Array.isArray(array)) throw new Error();
    if(array.length < 3) throw Error();
    if(array.some(ele => typeof ele !== "number")) throw Error();

    return array.reduce((prev, current, i) => {
        if(i + 2 < array.length && current === array[i+2] && array !== array[i+1])
            return prev + 1;
        return prev;
    }, 0);

}

const countBoomerangs1liner = (array) => !Array.isArray(array) ? (() => {throw new Error()})() : array.length < 3 ? (() => {throw new Error()})() : array.some(ele => typeof ele !== "number") ? (() => {throw new Error()})() : array.reduce((prev, current, i) => i + 2 < array.length && current === array[i+2] && current !== array[i+1] ? prev + 1 : prev, 0);
export default countBoomerangs1liner;