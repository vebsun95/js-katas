import countBoomerangs from './index';

test("...", () => {
    // Arrange
    let expected = 2;
    let array = [9, 5, 9, 5, 1, 1, 1];

    // Act
    let actual = countBoomerangs(array);

    expect(actual).toBe(expected);
});


test("...", () => {
    // Arrange
    let expected = 1;
    let array = [5, 6, 6, 7, 6, 3, 9];

    // Act
    let actual = countBoomerangs(array);

    expect(actual).toBe(expected);
});


test("...", () => {
    // Arrange
    let expected = 0;
    let array = [4, 4, 4, 9, 9, 9, 9]

    // Act
    let actual = countBoomerangs(array);

    expect(actual).toBe(expected);
});


test("...", () => {
    // Arrange
    let expected = 5;
    let array = [1, 7, 1, 7, 1, 7, 1];

    // Act
    let actual = countBoomerangs(array);

    expect(actual).toBe(expected);
});


test("...", () => {
    // Arrange
    let array = [];

    // Act && Assert
    expect(() => {countBoomerangs(array)}).toThrow();
});


test("...", () => {
    // Arrange
    let array = [1, 7];

    // Act && Assert
    expect(() => {countBoomerangs(array)}).toThrow();
});


test("...", () => {
    // Arrange
    let array = [1, 7, 1, 7, "one", 7, 1];

    // Act && Assert
    expect(() => {countBoomerangs(array)}).toThrow();
});