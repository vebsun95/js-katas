function secoundLargest(array) {
    if(!Array.isArray(array))
        throw new Error("Invalid argument type");
    if(array.length === 0 || (array.length === 2 && array[0] === array[1]))
        return 0;
    return array.sort((a, b) => a - b)[Math.max(array.length - 2, 0)];
}

const secoundLargestOneLine = (array) => array.length === 0 || (array.length === 2 && array[0] === array[1]) ? 0 : array.sort((a, b) => a - b)[Math.max(array.length - 2, 0)];

export default secoundLargestOneLine;