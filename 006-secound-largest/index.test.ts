import secoundLargest from ".";

test("...", () => {
    // Arrange
    let expected = 40;
    let array = [10, 40, 30, 20, 50];

    // Act
    let actual = secoundLargest(array);

    // Assert
    expect(actual).toBe(expected);
});


test("...", () => {
    // Arrange
    let expected = 105;
    let array = [25, 143, 89, 13, 105];

    // Act
    let actual = secoundLargest(array);

    // Assert
    expect(actual).toBe(expected);
});



test("...", () => {
    // Arrange
    let expected = 23;
    let array = [54, 23, 11, 17, 10];

    // Act
    let actual = secoundLargest(array);

    // Assert
    expect(actual).toBe(expected);
});


test("[1,1]", () => {
    // Arrange
    let expected = 0;
    let array = [1, 1];

    // Act
    let actual = secoundLargest(array);

    // Assert
    expect(actual).toBe(expected);
});

test("[1]", () => {
    // Arrange
    let expected = 1;
    let array = [1];

    // Act
    let actual = secoundLargest(array);

    // Assert
    expect(actual).toBe(expected);
});

test("[]", () => {
    // Arrange
    let expected = 0;
    let array = [];

    // Act
    let actual = secoundLargest(array);

    // Assert
    expect(actual).toBe(expected);
});