import isValidIp from ".";

test("..", () => {
    // Arrange
    let ipAdr = "1.2.3.4";
    let expected = true;

    // Act
    let actual = isValidIp(ipAdr);

    // Assert
    expect(actual).toBe(expected);
});

test("..", () => {
    // Arrange
    let ipAdr = "1.2.3";
    let expected = false;

    // Act
    let actual = isValidIp(ipAdr);

    // Assert
    expect(actual).toBe(expected);
});

test("..", () => {
    // Arrange
    let ipAdr = "1.2.3.4.5";
    let expected = false;

    // Act
    let actual = isValidIp(ipAdr);

    // Assert
    expect(actual).toBe(expected);
});

test("..", () => {
    // Arrange
    let ipAdr = "123.45.67.89";
    let expected = true;

    // Act
    let actual = isValidIp(ipAdr);

    // Assert
    expect(actual).toBe(expected);
});

test("..", () => {
    // Arrange
    let ipAdr = "123.456.78.90";
    let expected = false;

    // Act
    let actual = isValidIp(ipAdr);

    // Assert
    expect(actual).toBe(expected);
});

test("..", () => {
    // Arrange
    let ipAdr = "123.045.067.089";
    let expected = false;

    // Act
    let actual = isValidIp(ipAdr);

    // Assert
    expect(actual).toBe(expected);
});