function isValidIp(ipAdr) {
    let regEx = /([1-9][0-9]{0,3}).([1-9][0-9]{0,3}).([1-9][0-9]{0,3}).([1-9][0-9]{0,3})!*/;
    let match = ipAdr.match(regEx);
    if(!match || match[0] !== ipAdr)
        return false;
    for(let m of match.slice(1,5))
    {
        let number = Number(m);
        if(number < 1 || number > 255)
            return false;
    }
    return true;
}

export default isValidIp;