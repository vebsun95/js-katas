import caesarsCipher from './index'

test("...", () => {
    // Arrange 
    let word = 'middle-Outz'
    let shift = 2;
    let expected = 'okffng-Qwvb';

    // Act
    let actual = caesarsCipher(word, shift);

    expect(actual).toBe(expected);
});

test("...", () => {
    // Arrange 
    let word = 'Always-Look-on-the-Bright-Side-of-Life'
    let shift = 5;
    let expected = 'Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj';

    // Act
    let actual = caesarsCipher(word, shift);

    expect(actual).toBe(expected);
});

test("...", () => {
    // Arrange 
    let word = 'A friend in need is a friend indeed'
    let shift = 20;
    let expected = 'U zlcyhx ch hyyx cm u zlcyhx chxyyx';

    // Act
    let actual = caesarsCipher(word, shift);

    expect(actual).toBe(expected);
});