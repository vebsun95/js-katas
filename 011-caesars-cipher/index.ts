const A_ASCII_VALUE = 65;
const Z_ASCII_VALUE = 90;
const a_AZCII_VALUE = 97;
const z_AZCII_VALUE = 122;

const caesarsCipher = (word: string, shift: number) => word.split("")
        .reduce((cipher, char) => {
            let charCode = char.charCodeAt(0);
            let shiftedCharCode = charCode + shift;
            if(A_ASCII_VALUE <= charCode && charCode <= Z_ASCII_VALUE) {
                shiftedCharCode = shiftedCharCode <= Z_ASCII_VALUE ?
                    shiftedCharCode :
                    A_ASCII_VALUE + ((shiftedCharCode) % (Z_ASCII_VALUE + 1));
                cipher += String.fromCharCode(shiftedCharCode);
            }
            else if(a_AZCII_VALUE <= charCode && charCode <= z_AZCII_VALUE) {
                shiftedCharCode = shiftedCharCode <= z_AZCII_VALUE ?
                    shiftedCharCode :
                    a_AZCII_VALUE + ((shiftedCharCode) % (z_AZCII_VALUE + 1));
                cipher += String.fromCharCode(shiftedCharCode);
            }
            else {
                cipher += char;
            }
            return cipher;
        }, "");

export default caesarsCipher;