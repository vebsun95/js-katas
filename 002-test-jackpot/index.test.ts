import testJackPot from './index';

const outcomes = [
	["@", "@", "@", "@"],
	["abc", "abc", "abc", "abc"],
	["SS", "SS", "SS", "SS"],
	["&&", "&", "&&&", "&&&&"],
	["SS", "SS", "SS", "Ss"],
];

const expecteds = [true, true, true, false, false];
for(let i=0; i < outcomes.length; i++) {
	// Arrange
	const expected = expecteds[i];
	const outcome  = outcomes[i]

	// Act
	const actual = testJackPot(outcome);

	// Assertt
	test("...", () => {
		expect(actual).toBe(expected);
	})
}
