function testJackPot(outcome)
{
    if(!Array.isArray(outcome) || typeof outcome[0] != 'string')
        throw new Error("Invalid data type!");
        
    if(outcome.length != 4)
        throw new Error("An jackpot outcome has to have 4 items displayed");
     
    return outcome.every(x => x === outcome[0]);
    //return outcome.reduce(
        //(prev, current) => current === outcome[0] && prev,
        //true,
    //);
}

export default testJackPot;