function happyNumber(number: number) {
    let triedNumbers = new Set<number>()
        triedNumbers.add(number);
    let count = 1;

    function happyNumberRecursiv(num: number) {
        let digits = getDigits(num);
        let newNum = digits.reduce((sum, digit) => sum + digit * digit, 0);
        if(newNum === 1) {
            return `HAPPY ${count}`;
        }
        else if(triedNumbers.has(newNum)) {
            return `SAD ${count}`
        }
        count = count + 1;
        triedNumbers.add(newNum);
        return happyNumberRecursiv(newNum);
    }

    return happyNumberRecursiv(number);
}

function getDigits(number: number): number[] {
    return number
        .toString()
        .split("")
        .map(numAsString => Number(numAsString));
}

export default happyNumber;