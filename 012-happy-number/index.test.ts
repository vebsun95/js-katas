import happyNumber from ".";

test("67 should return SAD 10", () => {
    // Arrange
    let number = 67;
    let exected = "SAD 10";

    // Act
    let actual = happyNumber(number);

    // Assert
    expect(actual).toBe(exected);
});


test("139 should return HAPPY 5", () => {
    // Arrange
    let number = 139;
    let exected = "HAPPY 5";

    // Act
    let actual = happyNumber(number);

    // Assert
    expect(actual).toBe(exected);
});


test("1 should return HAPPY 1", () => {
    // Arrange
    let number = 1;
    let exected = "HAPPY 1";

    // Act
    let actual = happyNumber(number);

    // Assert
    expect(actual).toBe(exected);
});



test("89 should return SAD 8", () => {
    // Arrange
    let number = 89;
    let exected = "SAD 8";

    // Act
    let actual = happyNumber(number);

    // Assert
    expect(actual).toBe(exected);
});