function elasticize(word) {
    if(typeof word != "string")
        throw new Error("Invalid argument type");
    if(word.length < 3)
        return word;

    let center = Math.floor(word.length / 2);
    let res = "";

    for(let i = center * -1; i <= center; i++) {
        if(i === 0 && word.length % 2 === 0 ) continue;
        let multiplier = center - Math.abs(i) + 1;
        res += word[multiplier - 1].repeat(multiplier)
    }
    return res;
}

console.log(elasticize("ANNA"))
console.log("-------");
console.log(elasticize("KAYAK"));
