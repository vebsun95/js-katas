function howUnlucky(year) {
    // January 13.
    let date = new Date(year, 0, 13, 1, 1, 1);
    let nrOfFridayThe13 = 0;
    while(date.getFullYear() < year + 1) {
        if(date.getDay() === 5/* aka. friday */) {
            nrOfFridayThe13++;
        }
        date.setMonth(date.getMonth() + 1);
    }
    return nrOfFridayThe13;
}


export default howUnlucky;