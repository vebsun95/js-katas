import howUnlucky from ".";

test("2020 should have 2", () => {
    // Arrange
    let year = 2020;
    let expected = 2;

    // Act
    let actual = howUnlucky(year);

    // Assert
    expect(actual).toBe(expected);
});


test("2026 should have 3", () => {
    // Arrange
    let year = 2026;
    let expected = 3;

    // Act
    let actual = howUnlucky(year);

    // Assert
    expect(actual).toBe(expected);
});


test("2016 should have 1", () => {
    // Arrange
    let year = 2016;
    let expected = 1;

    // Act
    let actual = howUnlucky(year);

    // Assert
    expect(actual).toBe(expected);
});