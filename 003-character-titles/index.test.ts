import correctTitle from './index';

test("jOn SnoW, kINg IN thE noRth should be Jon Snow, King in the North.", () => {
	// Arrange
	let input = "jOn SnoW, kINg IN thE noRth";
	let expected = "Jon Snow, King in the North.";

	// Act
	let actual = correctTitle(input);

	// Assert
	expect(actual).toBe(expected);
});


test("sansa stark,lady of winterfell. should be Sansa Stark, Lady of Winterfell.", () => {
	// Arrange
	let input = "sansa stark,lady of winterfell.";
	let expected = "Sansa Stark, Lady of Winterfell.";

	// Act
	let actual = correctTitle(input);

	// Assert
	expect(actual).toBe(expected);
});


test("TYRION LANNISTER, HAND OF THE QUEEN. should be Tyrion Lannister, Hand of the Queen.", () => {
	// Arrange
	let input = "TYRION LANNISTER, HAND OF THE QUEEN.";
	let expected = "Tyrion Lannister, Hand of the Queen.";

	// Act
	let actual = correctTitle(input);

	// Assert
	expect(actual).toBe(expected);
});