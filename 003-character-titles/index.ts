const RESERVED_WORDS = ["and", "the", "of", "in"];

function correctTitle(title)
{
    if(typeof title != "string")
        throw new Error("Invalid type");

    // Insert space after all ','
    for(let i=0; i < title.length - 2; i++) {
        if(title[i] == ',' && title[i+1] != " ") {
            title = title.slice(0, i + 1) + " " + title.slice(i + 1);
        }
    }

    // corret case on words.
    let titleAsArray = title.split(" ");
    for(let i=0; i < titleAsArray.length; i++) {
        let word = titleAsArray[i];
        if(RESERVED_WORDS.includes(word.toLowerCase())) {
            titleAsArray[i] = word.toLowerCase();
        }
        else {
            titleAsArray[i] = word[0].toUpperCase() + word.slice(1).toLowerCase();
        }
    }
    title = titleAsArray.join(" ");

    // Add '.' at the end
    if(title[title.length - 1] != ".") title += ".";
    return title;
}

export default correctTitle;