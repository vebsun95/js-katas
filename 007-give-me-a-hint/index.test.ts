import grantTheHint from ".";

test("...", () => {
    // Arrange
    const sentence = 'Mary Queen of Scots';
    const expected = [
        "____ _____ __ _____",
        "M___ Q____ o_ S____",
        "Ma__ Qu___ of Sc___",
        "Mar_ Que__ of Sco__",
        "Mary Quee_ of Scot_",
        "Mary Queen of Scots"];
    
    // Act
    const actual = grantTheHint(sentence);

    // Assert
    expect(actual).toStrictEqual(expected);
});

test("...", () => {
    // Arrange
    const sentence = 'The Life of Pi';
    const expected = [
        "___ ____ __ __",
        "T__ L___ o_ P_",
        "Th_ Li__ of Pi",
        "The Lif_ of Pi",
        "The Life of Pi"];
    
    // Act
    const actual = grantTheHint(sentence);

    // Assert
    expect(actual).toStrictEqual(expected);
});