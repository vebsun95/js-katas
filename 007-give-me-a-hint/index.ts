function grantTheHint(sentence) {
    let words = sentence.split(" ");
    let maxNrOfIterations = Math.max(...words.map(w => w.length)) + 1;
    function grantHintRec(hints, nrOfIterations) {
        if(nrOfIterations >= maxNrOfIterations)
            return hints;
        let newHint = [...hints[0]];
        for(let i=0; i < words.length; i++) {
            let word= words[i];
            newHint[i] = word.slice(0, nrOfIterations) + newHint[i].slice(nrOfIterations);
        }
        hints.push(newHint);
        return grantHintRec(hints, ++nrOfIterations)
    }
    return grantHintRec([words.map(word => word.replace(/[a-z]/gi, "_"))], 1).map(hint => hint.join(" "));
}

export default grantTheHint;