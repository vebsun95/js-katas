import charCount from './index';

test("......", () => {
	// Arrange
	let target = "edabit";
    let character = 'a';
    let expected = 1;

	// Act
	let actual = charCount(character, target);

	// Assert
	expect(actual).toBe(expected);
});
test("......", () => {
	// Arrange
	let target = "Chamber of secrets";
    let character = 'c';
    let expected = 1;

	// Act
	let actual = charCount(character, target);

	// Assert
	expect(actual).toBe(expected);
});
test("......", () => {
	// Arrange
	let target = "boxes are fun";
    let character = 'B';
    let expected = 0;

	// Act
	let actual = charCount(character, target);

	// Assert
	expect(actual).toBe(expected);
});
test("......", () => {
	// Arrange
	let target = "big far bubble";
    let character = 'b';
    let expected = 4;

	// Act
	let actual = charCount(character, target);

	// Assert
	expect(actual).toBe(expected);
});
test("......", () => {
	// Arrange
	let target = "javascript is good";
    let character = 'e';
    let expected = 0;

	// Act
	let actual = charCount(character, target);

	// Assert
	expect(actual).toBe(expected);
});
test("......", () => {
	// Arrange
	let target = "!easy!";
    let character = '!';
    let expected = 2;

	// Act
	let actual = charCount(character, target);

	// Assert
	expect(actual).toBe(expected);
});
test("......", () => {
	// Arrange
	let target = "the universe is wow";
    let character = 'wow';

    // Act && Assert
    expect(() => charCount(character, target)).toThrow();
});