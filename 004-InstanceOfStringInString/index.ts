function charCount(char, target) { 
    if(typeof char != 'string')
        throw Error("char has to be of type string not " + typeof char);
    if(typeof target != 'string')
        throw Error("target has to be of type string not " + typeof target);
    if(char.length != 1) 
        throw Error("Char can only be of lenght 1");
    return (target.match(new RegExp(char, 'g')) || []).length;
}

export default charCount;