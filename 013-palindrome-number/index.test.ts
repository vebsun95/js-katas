import isPandigital from ".";

test("...", () => {
    // Arrange
    let number = 98140723568910;
    let expected = true;

    // Act
    let actual = isPandigital(number);

    // Assert
    expect(actual).toBe(expected);
});



test("90864523148909 should fail, missing 7", () => {
    // Arrange
    let number = 90864523148909;
    let expected = false;

    // Act
    let actual = isPandigital(number);

    // Assert
    expect(actual).toBe(expected);
});



test("112233445566778899 should fail, missing 0...", () => {
    // Arrange
    let number = 112233445566778899;
    let expected = false;

    // Act
    let actual = isPandigital(number);

    // Assert
    expect(actual).toBe(expected);
});