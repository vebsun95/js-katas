const isPandigital = (number: number) => new Set(BigInt(number).toString().split("")).size === 10;

export default isPandigital;